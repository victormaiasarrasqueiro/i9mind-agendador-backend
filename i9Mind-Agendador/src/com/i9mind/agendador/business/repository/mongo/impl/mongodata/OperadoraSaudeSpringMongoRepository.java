package com.i9mind.agendador.business.repository.mongo.impl.mongodata;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.i9mind.agendador.business.entity.OperadoraSaude;

@Repository
public interface OperadoraSaudeSpringMongoRepository extends MongoRepository<OperadoraSaude, Long> {

}
