package com.i9mind.agendador.business.repository.mongo.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.i9mind.agendador.business.entity.Especialidade;
import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.business.repository.mongo.EspecialidadeMongoRepository;
import com.i9mind.agendador.business.repository.mongo.impl.mongodata.EspecialidadeSpringMongoRepository;
import com.i9mind.agendador.comum.exception.IntegrationException;


/**
 * 
 * DocumentBase
 * 
 * @author i9Mind - Victor Sarrasqueiro
 *
 */
@Repository
public class EspecialidadeMongoRepositoryImpl implements EspecialidadeMongoRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EspecialidadeMongoRepositoryImpl.class);

	@Autowired
	EspecialidadeSpringMongoRepository mongoRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;

	
	@Override
	public List<?> listar(DocumentBase pDocumentBase)  {
		
		try
		{
			Query query = new Query(Criteria.where("idEmp").is(pDocumentBase.getIdEmpresa()).and("ieAtv").is(true));
			query.fields().include("id").include("nmEsp");
			query.with(new Sort(Sort.Direction.ASC, "nmEsp"));
			
			return mongoTemplate.find(query, Especialidade.class);
		}
		catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao listar" + e.getMessage());
			throw new IntegrationException("Erro ao listar.", e);
		}

	};
	
	@Override
	public DocumentBase obter(DocumentBase pDocumentBase) {
		
		try
		{
			Query query = new Query();
			query.addCriteria(Criteria.where("id").is(pDocumentBase.getId()).and("idEmp").is(pDocumentBase.getIdEmpresa()));
			
			return mongoTemplate.findOne(query, Especialidade.class);
		}
		catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao obter" + e.getMessage());
			throw new IntegrationException("Erro ao obter.", e);
		}
		
	};
	
	@Override
	public DocumentBase inserir(DocumentBase pDocumentBase) {
		
		try
		{
			return mongoRepository.insert( (Especialidade) pDocumentBase );
			
		}catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao inserir" + e.getMessage());
			throw new IntegrationException("Erro ao inserir.", e);
		}
		
	};
	
	@Override
	public void atualizar(DocumentBase pDocumentBase) {
		
		try
		{
			
			mongoRepository.save( (Especialidade) pDocumentBase );
			
		}catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao atualizar" + e.getMessage());
			throw new IntegrationException("Erro ao atualizar.", e);
		}
	};
	
	@Override
	public void inativar(DocumentBase pDocumentBase) {
		
		try
		{
			pDocumentBase.setAtivo(false);
			mongoRepository.save( (Especialidade) pDocumentBase );
		}
		catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao inativar" + e.getMessage());
			throw new IntegrationException("Erro ao inativar.", e);
		}
		
	}
	
	@Override
	public void ativar(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			pDocumentBase.setAtivo(true);
			mongoRepository.save( (Especialidade) pDocumentBase );
		}
		catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao ativar" + e.getMessage());
			throw new IntegrationException("Erro ao ativar.", e);
		}
		
	}

	@Override
	public void excluir(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			mongoRepository.delete( (Especialidade) pDocumentBase );
		}
		catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao excluir" + e.getMessage());
			throw new IntegrationException("Erro ao excluir.", e);
		}
		
	};

	@Override
	public List<?> listarInativo(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			Query query = new Query(Criteria.where("idEmp").is(pDocumentBase.getIdEmpresa()).and("ieAtv").is(false));
			query.fields().include("id").include("nmEsp");
			query.with(new Sort(Sort.Direction.ASC, "nmEsp"));
			
			return mongoTemplate.find(query, Especialidade.class);
		}
		catch(Exception e)
		{
			LOGGER.error("EspecialidadeMongoRepositoryImpl - Erro ao listarInativo" + e.getMessage());
			throw new IntegrationException("Erro ao listarInativo.", e);
		}
		
	};

};
