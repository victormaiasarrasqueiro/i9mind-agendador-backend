package com.i9mind.agendador.business.document;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Id;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */

@Document(collection="agendamento")
@JsonInclude(Include.NON_NULL)
public class AgendamentoDoc implements Serializable  {

	private static final long serialVersionUID = 5307977634091710690L;

	@Id
	private BigInteger id;
	
	@NotNull
	@Future
	@JsonProperty(value = "dtIni")
	private Date dataInicioAgendamento;     
    
	@NotNull
	@Future
	@JsonProperty(value = "dtFim")
	private Date dataFimAgendamento;     

	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	
	

}

