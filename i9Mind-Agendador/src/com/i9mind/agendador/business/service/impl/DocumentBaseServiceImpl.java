package com.i9mind.agendador.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.business.repository.mongo.DocumentBaseMongoRepository;
import com.i9mind.agendador.business.service.DocumentBaseService;
import com.i9mind.agendador.comum.exception.IntegrationException;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class DocumentBaseServiceImpl extends BaseService implements DocumentBaseService {

	
	DocumentBaseMongoRepository repository;
	
	/**
	 * 
	 * Listar Todos os Registros da Entidade passada.
	 * 
	 * @return List<T> - Lista.
	 * 
	 */
	public List<?> listar(DocumentBase pDocumentBase){
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa.
			pDocumentBase.setIdEmpresa(getIDEmpresa());
			
			return repository.listar(pDocumentBase);
			
		}
		catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "listar" , "Erro ao listar", e);
		}
		
	};
	
	@Override
	public DocumentBase obter(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa logada.
			pDocumentBase.setIdEmpresa(getIDEmpresa());
			
			return repository.obter(pDocumentBase);

		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "obter" , "Erro ao remover especialidade", e);
		}	
		
	};
	
	/**
	 * Inserindo um novo registro no sistema.
	 */
	public String inserir(DocumentBase pDocumentBase){
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa logada.
			pDocumentBase.setIdEmpresa(getIDEmpresa());
			
			//Recuperando o ID do usu�rio logado.
			pDocumentBase.setIdUsuarioCadadastro(getIDUsuarioCadastro());
			
			//Recuperando a data atual.
			pDocumentBase.setDataCadastroSistema(new Date());
			
			//Definindo o status como true.
			pDocumentBase.setAtivo(true);

			//Inserindo no MongoDB
			DocumentBase retorno = repository.inserir(pDocumentBase);
			
			//Retornando ID MongoDB
			return retorno.getId();
			
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "inserir" , "Erro ao inserir especialidade", e);
		}	
	
	};
	
	public void atualizar(DocumentBase pDocumentBase){
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa logada.
			pDocumentBase.setIdEmpresa(getIDEmpresa());
			
			//Definindo o status como true.
			pDocumentBase.setAtivo(true);
			
			repository.atualizar(pDocumentBase);
		
		}
		catch(Exception e)
		{
			throw new IntegrationException("DocumentBaseServiceImpl", "atualizar" , "Erro ao inserir especialidade", e);
		}	
		
	};
	
	@Override
	public void excluir(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa logada.
			pDocumentBase.setIdEmpresa(getIDEmpresa());

			//Inativando o especialidade no MONGO
			repository.excluir(pDocumentBase);

		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "excluir" , "Erro ao remover especialidade", e);
		}	
		
	};
	
	/**
	 * 
	 * Alterar o status do registro de ATIVO para INATIVO.
	 * 
	 */
	public void inativar(DocumentBase pDocumentBase){
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa logada.
			pDocumentBase.setIdEmpresa(getIDEmpresa());
			
			DocumentBase especialidadeRecuperado = repository.obter(pDocumentBase);
			
			//Inativando o especialidade no MONGO
			repository.inativar(especialidadeRecuperado);
			
			//Inativando o especialidade no MYSQL
			//instrucao.
		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "inativar" , "Erro ao remover especialidade", e);
		}	
		
	};
	
	@Override
	public void ativar(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa logada.
			pDocumentBase.setIdEmpresa(getIDEmpresa());

			//Inativando o especialidade no MONGO
			repository.ativar(pDocumentBase);

		}
    	catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "ativar" , "Erro ao remover especialidade", e);
		}	
		
	}

	@Override
	public List<?> listarInativo(DocumentBase pDocumentBase) throws IntegrationException {
		
		try
		{
			repository = getRepository(pDocumentBase);
			
			//Recuperando o ID da empresa.
			pDocumentBase.setIdEmpresa(getIDEmpresa());
			
			return repository.listarInativo(pDocumentBase);
			
		}
		catch(Exception e)
    	{
			throw new IntegrationException("DocumentBaseServiceImpl", "listarInativo" , "Erro ao listar", e);
		}
		
	};

};