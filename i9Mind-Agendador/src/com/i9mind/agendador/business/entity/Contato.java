package com.i9mind.agendador.business.entity;

import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@JsonInclude(Include.NON_NULL)
public class Contato implements Serializable{

	private static final long serialVersionUID = 1263763134184474989L;
	
	@Field("nuCel")
	@Size(min=8, max=18)
	@JsonProperty(value = "nuCel")
	private String numeroCelular;
	
	@DecimalMin(value = "0")
	@JsonProperty(value = "idOCe")
	private Integer idOperadoraCelular;  
	
	@Size(min=7, max=18)
	@JsonProperty(value = "nuTel")
	private String numeroTelefone;

	@Size(min=2, max=30)
	@JsonProperty(value = "nmRTe")
	private String nomeRecadoTelefone;

	@Size(min=8, max=18)
	@JsonProperty(value = "nuWap")
	private String numeroWhatsapp;

	@Size(min=8, max=40)
	@JsonProperty(value = "dsEma")
	private String email;
	
	@Size(min=3, max=100)
	@JsonProperty(value = "dsOct")
	private String outrosContatos;

	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	
	
	public Integer getIdOperadoraCelular() {
		return idOperadoraCelular;
	}

	public void setIdOperadoraCelular(Integer idOperadoraCelular) {
		this.idOperadoraCelular = idOperadoraCelular;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public String getNomeRecadoTelefone() {
		return nomeRecadoTelefone;
	}

	public void setNomeRecadoTelefone(String nomeRecadoTelefone) {
		this.nomeRecadoTelefone = nomeRecadoTelefone;
	}

	public String getNumeroWhatsapp() {
		return numeroWhatsapp;
	}

	public void setNumeroWhatsapp(String numeroWhatsapp) {
		this.numeroWhatsapp = numeroWhatsapp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getOutrosContatos() {
		return outrosContatos;
	}

	public void setOutrosContatos(String outrosContatos) {
		this.outrosContatos = outrosContatos;
	}

}

