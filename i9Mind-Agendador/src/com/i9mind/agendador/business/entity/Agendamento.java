package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */

@Entity
@JsonInclude(Include.NON_NULL)
public class Agendamento implements Serializable  {

	private static final long serialVersionUID = 5307977634091710690L;

	@Id
	@JsonProperty(value = "id")
	private BigInteger id;
	
	@NotNull
	@Future
	@JsonProperty(value = "dtIni")
	private Date dataInicioAgendamento;     
    
	@NotNull
	@Future
	@JsonProperty(value = "dtFim")
	private Date dataFimAgendamento;     
	
	@NotNull
	@JsonProperty(value = "cliente")
	private Cliente cliente; 
	
	@JsonProperty(value = "profissional")
	private Profissional profissional;
	
	@JsonProperty(value = "servico")
	private Servico servico;


	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Date getDataInicioAgendamento() {
		return dataInicioAgendamento;
	}

	public void setDataInicioAgendamento(Date dataInicioAgendamento) {
		this.dataInicioAgendamento = dataInicioAgendamento;
	}

	public Date getDataFimAgendamento() {
		return dataFimAgendamento;
	}

	public void setDataFimAgendamento(Date dataFimAgendamento) {
		this.dataFimAgendamento = dataFimAgendamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Profissional getProfissional() {
		return profissional;
	}

	public void setProfissional(Profissional profissional) {
		this.profissional = profissional;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

