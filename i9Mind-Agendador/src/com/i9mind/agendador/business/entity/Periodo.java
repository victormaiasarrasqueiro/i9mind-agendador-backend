package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@JsonInclude(Include.NON_NULL)
public class Periodo implements Serializable{

	private static final long serialVersionUID = 2346519996274210748L;

	@JsonProperty(value = "idDay")
	private Integer idDay;
    
	@JsonProperty(value = "start")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date start;        
	
	@JsonProperty(value = "end")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date end;

	public Integer getIdDay() {
		return idDay;
	}

	public void setIdDay(Integer idDay) {
		this.idDay = idDay;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}        

}

