package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */

@Entity
@JsonInclude(Include.NON_NULL)
public class Usuario implements Serializable  {

	private static final long serialVersionUID = 7557037754014057104L;

	@Id
	@JsonProperty(value = "id")
	private BigInteger id;
	
	@NotNull
	@JsonProperty(value = "tpUsu")
	private TipoUsuario tipoUsuario;
	
	@NotNull
	@Size(min=2, max=15)
	@JsonProperty(value = "nmPri")
	private String nomePrimeiro;     
    
	@NotNull
	@Size(min=2, max=40)
	@JsonProperty(value = "nmUlt")
	private String nomeUltimo;         
	
	@Past
	@NotNull
	@JsonProperty(value = "dtNas")
	private Date dataNascimento;        

	@DecimalMin(value = "0")
	@DecimalMax(value = "2")
	@JsonProperty(value = "tpSex")
	private Byte tipoSexo;       		// Sexo  ( 0 - Não Informado   1 - Masculino   2 - Feminino  )
	
	@Size(min=8, max=11)
	@JsonProperty(value = "nuIde")
	private String numeroIdentidade;    
	
	@Size(min=8, max=13)
	@JsonProperty(value = "nuCpf")
	private String numeroCpf;    		
	
	@Size(min=8, max=18)
	@JsonProperty(value = "nuCel")
	private String numeroCelular;
	
	@Size(min=7, max=18)
	@JsonProperty(value = "nuTel")
	private String numeroTelefone;
	
	@Size(min=8, max=18)
	@JsonProperty(value = "nuWap")
	private String numeroWhatsapp;
	
	@Size(min=8, max=40)
	@JsonProperty(value = "dsEma")
	private String email;
	
	@Size(min=5, max=9)
	@JsonProperty(value = "nuCep")
	private String numeroCEP;
	
	@Size(min=3, max=40)
	@JsonProperty(value = "dsLog")
	private String logradouro;
	
	@Size(min=1, max=10)
	@JsonProperty(value = "nuLog")
	private String numeroLogradouro;
	
	@Size(min=2, max=20)
	@JsonProperty(value = "dsBai")
	private String bairro;
	
	@Size(min=2, max=20)
	@JsonProperty(value = "dsCid")
	private String cidade;
	
	@Size(min=2, max=20)
	@JsonProperty(value = "dsEst")
	private String estado;
	
	@Size(min=2, max=20)
	@JsonProperty(value = "dsPai")
	private String pais;
	
	@Past
	@JsonIgnore
	@JsonProperty(value = "dtCad")
	private Date dataCadastro;
	
	@Past
	@JsonIgnore
	@JsonProperty(value = "dtDes")
	private Date dataDesativamento;

	@JsonIgnore
	private Boolean ativo;

	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getNomePrimeiro() {
		return nomePrimeiro;
	}

	public void setNomePrimeiro(String nomePrimeiro) {
		this.nomePrimeiro = nomePrimeiro;
	}

	public String getNomeUltimo() {
		return nomeUltimo;
	}

	public void setNomeUltimo(String nomeUltimo) {
		this.nomeUltimo = nomeUltimo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Byte getTipoSexo() {
		return tipoSexo;
	}

	public void setTipoSexo(Byte tipoSexo) {
		this.tipoSexo = tipoSexo;
	}

	public String getNumeroIdentidade() {
		return numeroIdentidade;
	}

	public void setNumeroIdentidade(String numeroIdentidade) {
		this.numeroIdentidade = numeroIdentidade;
	}

	public String getNumeroCpf() {
		return numeroCpf;
	}

	public void setNumeroCpf(String numeroCpf) {
		this.numeroCpf = numeroCpf;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public String getNumeroWhatsapp() {
		return numeroWhatsapp;
	}

	public void setNumeroWhatsapp(String numeroWhatsapp) {
		this.numeroWhatsapp = numeroWhatsapp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeroCEP() {
		return numeroCEP;
	}

	public void setNumeroCEP(String numeroCEP) {
		this.numeroCEP = numeroCEP;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataDesativamento() {
		return dataDesativamento;
	}

	public void setDataDesativamento(Date dataDesativamento) {
		this.dataDesativamento = dataDesativamento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

