package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.i9mind.agendador.business.entity.base.DocumentBase;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@Document(collection="especialidades")
@JsonInclude(Include.NON_NULL)
public class Especialidade extends DocumentBase implements Serializable  {

	private static final long serialVersionUID = -7022311630979288884L;
	
	@Id
	@JsonProperty(value = "id")
	private String id;

	
	@Indexed
	@Field("idEmp")
	@JsonIgnore
	private String idEmpresa;

	@Field("idUcd")
	@JsonIgnore
	private BigInteger idUsuarioCadadastro;
	
	@Field("dtCad")
	@JsonIgnore
	private Date dataCadastroSistema;      
	
	@Field("ieAtv")
	@JsonIgnore
	private Boolean ativo;
	
	//**** FILDS ****//
	
	@TextIndexed
	@Field("nmEsp")
	@NotNull
	@Size(min=3, max=60)
	@JsonProperty(value = "nmEsp")
	private String nomeEspecialidade;
	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	public String getId() {
		return id;
	}

	public String getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNomeEspecialidade() {
		return nomeEspecialidade;
	}

	public void setNomeEspecialidade(String nomeEspecialidade) {
		this.nomeEspecialidade = nomeEspecialidade;
	}

	public BigInteger getIdUsuarioCadadastro() {
		return idUsuarioCadadastro;
	}

	public void setIdUsuarioCadadastro(BigInteger idUsuarioCadadastro) {
		this.idUsuarioCadadastro = idUsuarioCadadastro;
	}

	public Date getDataCadastroSistema() {
		return dataCadastroSistema;
	}

	public void setDataCadastroSistema(Date dataCadastroSistema) {
		this.dataCadastroSistema = dataCadastroSistema;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setId(String id) {
		this.id = id;
	}

}

