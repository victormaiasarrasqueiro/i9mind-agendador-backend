package com.i9mind.agendador.business.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Victor Sarrasqueiro - i9Mind Solutions
 * @mail victormaiasarrasqueiro@gmail.com
 * 
 */
@Document(collection="clientes")
@JsonInclude(Include.NON_NULL)
public class Cliente extends DocumentBase implements Serializable{

	private static final long serialVersionUID = 3752180652461418180L;

	@Id
	@JsonProperty(value = "id")
	private String id;

	@Indexed
	@Field("idEmp")
	@JsonIgnore
	private String idEmpresa;

	@TextIndexed
	@Field("nmCli")
	@NotNull
	@Size(min=3, max=50)
	@JsonProperty(value = "nmCli")
	private String nomeCliente;
    
	@TextIndexed
	@Field("nuCel")
	@Size(min=8, max=18)
	@JsonProperty(value = "nuCel")
	private String numeroCelular;

	@TextIndexed
	@Field("nuIde")
	@Size(min=8, max=11)
	@JsonProperty(value = "nuIde")
	private String numeroIdentidade;

	@Field("dtNas")
	@Past
	@JsonProperty(value = "dtNas")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date dataNascimento;        
	
	@Field("tpSex")
	@DecimalMin(value = "0")
	@DecimalMax(value = "2")
	@JsonProperty(value = "tpSex")
	private Byte tipoSexo;       		// Sexo  ( 0 - Não Informado   1 - Masculino   2 - Feminino  )

	@Field("isRes")
	@NotNull
	@JsonProperty(value = "isRes")
	private Boolean isResponsavel;   	// Responsável ( TRUE - SIM, é seu proprio responsavel    FALSE- NÂO, POSSUI RESPONSAVEL  )

	@Field("isDef")
	@NotNull
	@JsonProperty(value = "isDef")
	private Boolean isDeficiente;   	// Deficiência ( TRUE - POSSUI    FALSE- NÂO POSSUI  )

	@Field("nuCpf")
	@Size(min=8, max=13)
	@JsonProperty(value = "nuCpf")
	private String numeroCpf;    		// Numero do CPF do Cliente

	@Field("tpECi")
	@DecimalMin(value = "0")
	@DecimalMax(value = "6")
	@JsonProperty(value = "tpECi")
	private Byte tipoEstadoCivil;    	// Estado Civil ( 0 - Não Informado  1 - Solteiro(a)  2 - Casado(a) 3 - Divorciado(a) 4 -Viúvo(a)  5 - Separado(a)  6 - Companheiro(a) )  

	@Field("tpCon")
	@DecimalMin(value = "0")
	@DecimalMax(value = "5")
	@JsonProperty(value = "tpCon")
	private Byte tipoConvenio;      	// Tipo de Convênio  ( 0 - Sem Convênio   1 - Operadora de Saude   2 - Empresas   3 - ONG     4- Trabalho Social   5- Outros  )  
	
	@Field("dsOER")
	@Size(min=3, max=10)
	@JsonProperty(value = "dsOER")
	private String dsOrgaoEmissor;

	@Field("idOEU")
	@DecimalMin(value = "0")
	@DecimalMax(value = "2")
	@JsonProperty(value = "idOEU")
	private Byte idOrgaoEmissorUF;    
	
	//##### Dados para o cadastro de operadora de saude.######//
	
	@Field("idOsa")
	@DecimalMin(value = "0")
	@JsonProperty(value = "idOsa")
	private Integer idOperadoraSaude;      	
	
	@Field("dsOpc")
	@Size(min=2, max=20)
	@JsonProperty(value = "dsOpc")
	private String operadoraSaudePlanoContradado;
	
	@Field("nuOpc")
	@Size(min=2, max=30)
	@JsonProperty(value = "nuOpc")
	private String operadoraSaudeNumeroPlanoContradado;
	
	
	//##### Dados para o cadastro de Resonsavel ######//
	
	@Field("idRGp")
	@DecimalMin(value = "0")
	@DecimalMax(value = "5")
	@JsonProperty(value = "idRGp")
	private Integer idResponsavelGrauParentestico;   // 0 - Não Informado  1- 1º Grau  2- 2º Grau  3- 3º Grau  4- 4º Grau  5- Sem Parentesco
	
	@Field("nmRep")
	@Size(min=2, max=30)
	@JsonProperty(value = "nmRep")
	private String nomeResponsavel;
	
	@Field("nuIdR")
	@Size(min=8, max=11)
	@JsonProperty(value = "nuIdR")
	private String numeroIdentidadeResponsavel;

	
	
	//##### Contato do Cliente ######//
	
	@Field("idOCe")
	@DecimalMin(value = "0")
	@JsonProperty(value = "idOCe")
	private Integer idOperadoraCelular;  
	
	@Field("nuTel")
	@Size(min=7, max=18)
	@JsonProperty(value = "nuTel")
	private String numeroTelefone;

	@Field("nmRTe")
	@Size(min=2, max=30)
	@JsonProperty(value = "nmRTe")
	private String nomeRecadoTelefone;

	@Field("nuWap")
	@Size(min=8, max=18)
	@JsonProperty(value = "nuWap")
	private String numeroWhatsapp;

	@Field("dsEma")
	@Size(min=8, max=40)
	@JsonProperty(value = "dsEma")
	private String email;

	
	
	//##### Endereço ######//
	
	@Field("nuCep")
	@Size(min=5, max=9)
	@JsonProperty(value = "nuCep")
	private String numeroCEP;
	
	@Field("dsLog")
	@Size(min=3, max=40)
	@JsonProperty(value = "dsLog")
	private String logradouro;

	@Field("nuLog")
	@Size(min=1, max=10)
	@JsonProperty(value = "nuLog")
	private String numeroLogradouro;
	
	@Field("dsLoC")
	@Size(min=1, max=70)
	@JsonProperty(value = "dsLoC")
	private String complementoLogradouro;

	@Field("dsBai")
	@Size(min=2, max=30)
	@JsonProperty(value = "dsBai")
	private String bairro;

	@Field("dsCid")
	@Size(min=2, max=30)
	@JsonProperty(value = "dsCid")
	private String cidade;

	@Field("dsEst")
	@Size(min=2, max=30)
	@JsonProperty(value = "dsEst")
	private String estado;

	@Field("dsPai")
	@Size(min=2, max=20)
	@JsonProperty(value = "dsPai")
	private String pais;
	
	@Field("idUcd")
	@JsonProperty(value = "idUcd")
	private BigInteger idUsuarioCadadastro;
	
	@Field("dtCad")
	@JsonProperty(value = "dtCad")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date dataCadastroSistema;      
	
	@Field("ieAtv")
	@JsonProperty(value = "ieAtv")
	private Boolean ativo;

	
	/**
	 *
	 * Metodos GET's and SET's
	 * 
	 */
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getIdOperadoraSaude() {
		return idOperadoraSaude;
	}

	public void setIdOperadoraSaude(Integer idOperadoraSaude) {
		this.idOperadoraSaude = idOperadoraSaude;
	}

	public String getOperadoraSaudePlanoContradado() {
		return operadoraSaudePlanoContradado;
	}

	public void setOperadoraSaudePlanoContradado(String operadoraSaudePlanoContradado) {
		this.operadoraSaudePlanoContradado = operadoraSaudePlanoContradado;
	}

	public String getOperadoraSaudeNumeroPlanoContradado() {
		return operadoraSaudeNumeroPlanoContradado;
	}

	public void setOperadoraSaudeNumeroPlanoContradado(String operadoraSaudeNumeroPlanoContradado) {
		this.operadoraSaudeNumeroPlanoContradado = operadoraSaudeNumeroPlanoContradado;
	}

	public Integer getIdResponsavelGrauParentestico() {
		return idResponsavelGrauParentestico;
	}

	public void setIdResponsavelGrauParentestico(Integer idResponsavelGrauParentestico) {
		this.idResponsavelGrauParentestico = idResponsavelGrauParentestico;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public String getNumeroIdentidadeResponsavel() {
		return numeroIdentidadeResponsavel;
	}

	public void setNumeroIdentidadeResponsavel(String numeroIdentidadeResponsavel) {
		this.numeroIdentidadeResponsavel = numeroIdentidadeResponsavel;
	}

	public Integer getIdOperadoraCelular() {
		return idOperadoraCelular;
	}

	public void setIdOperadoraCelular(Integer idOperadoraCelular) {
		this.idOperadoraCelular = idOperadoraCelular;
	}

	public String getNomeRecadoTelefone() {
		return nomeRecadoTelefone;
	}

	public BigInteger getIdUsuarioCadadastro() {
		return idUsuarioCadadastro;
	}

	public void setIdUsuarioCadadastro(BigInteger idUsuarioCadadastro) {
		this.idUsuarioCadadastro = idUsuarioCadadastro;
	}

	public Date getDataCadastroSistema() {
		return dataCadastroSistema;
	}

	public void setDataCadastroSistema(Date dataCadastroSistema) {
		this.dataCadastroSistema = dataCadastroSistema;
	}
	
	public void setNomeRecadoTelefone(String nomeRecadoTelefone) {
		this.nomeRecadoTelefone = nomeRecadoTelefone;
	}

	public String getComplementoLogradouro() {
		return complementoLogradouro;
	}

	public void setComplementoLogradouro(String complementoLogradouro) {
		this.complementoLogradouro = complementoLogradouro;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getNumeroIdentidade() {
		return numeroIdentidade;
	}

	public void setNumeroIdentidade(String numeroIdentidade) {
		this.numeroIdentidade = numeroIdentidade;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Byte getTipoSexo() {
		return tipoSexo;
	}

	public void setTipoSexo(Byte tipoSexo) {
		this.tipoSexo = tipoSexo;
	}

	public Byte getTipoEstadoCivil() {
		return tipoEstadoCivil;
	}

	public void setTipoEstadoCivil(Byte tipoEstadoCivil) {
		this.tipoEstadoCivil = tipoEstadoCivil;
	}

	public Boolean getIsResponsavel() {
		return isResponsavel;
	}

	public void setIsResponsavel(Boolean isResponsavel) {
		this.isResponsavel = isResponsavel;
	}

	public Boolean getIsDeficiente() {
		return isDeficiente;
	}

	public void setIsDeficiente(Boolean isDeficiente) {
		this.isDeficiente = isDeficiente;
	}

	public String getNumeroCpf() {
		return numeroCpf;
	}

	public void setNumeroCpf(String numeroCpf) {
		this.numeroCpf = numeroCpf;
	}

	public Byte getTipoConvenio() {
		return tipoConvenio;
	}

	public void setTipoConvenio(Byte tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public String getNumeroWhatsapp() {
		return numeroWhatsapp;
	}

	public void setNumeroWhatsapp(String numeroWhatsapp) {
		this.numeroWhatsapp = numeroWhatsapp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeroCEP() {
		return numeroCEP;
	}

	public void setNumeroCEP(String numeroCEP) {
		this.numeroCEP = numeroCEP;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getDsOrgaoEmissor() {
		return dsOrgaoEmissor;
	}

	public void setDsOrgaoEmissor(String dsOrgaoEmissor) {
		this.dsOrgaoEmissor = dsOrgaoEmissor;
	}

	public Byte getIdOrgaoEmissorUF() {
		return idOrgaoEmissorUF;
	}

	public void setIdOrgaoEmissorUF(Byte idOrgaoEmissorUF) {
		this.idOrgaoEmissorUF = idOrgaoEmissorUF;
	}

}

