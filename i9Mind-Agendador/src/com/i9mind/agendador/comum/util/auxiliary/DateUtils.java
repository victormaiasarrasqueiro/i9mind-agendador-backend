package com.i9mind.agendador.comum.util.auxiliary;

import java.util.Date;

public class DateUtils {
	
	
	public static boolean isMaiorHoje(Date d){
		
		Date hj = new Date();
		return d.after(hj);
		
	}
	
	public static boolean isMenorHoje(Date d){
		
		Date hj = new Date();
		return d.before(hj);
		
	}

}
