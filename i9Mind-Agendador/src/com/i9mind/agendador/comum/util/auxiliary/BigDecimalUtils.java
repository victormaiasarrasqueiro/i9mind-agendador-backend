package com.i9mind.agendador.comum.util.auxiliary;

import java.math.BigDecimal;

public class BigDecimalUtils extends ObjectUtils{
	
	
	public static boolean isNotEmpty(BigDecimal number){
		
		if(isNotNull(number) && !number.toString().trim().equals("0")){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static boolean isEmpty(BigDecimal number){
		
		if(isNull(number) || number.toString().trim().equals("0")){
			return true;
		}else{
			return false;
		}
		
	}

}
