package com.i9mind.agendador.integration.service.rest.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.i9mind.agendador.business.entity.base.DocumentBase;
import com.i9mind.agendador.business.entity.util.HttpResponse;
import com.i9mind.agendador.business.service.DocumentBaseService;
import com.i9mind.agendador.comum.util.auxiliary.StringUtils;


public class BaseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseRestController.class);
	 
	@Autowired
	private DocumentBaseService service;
	
	
	/**
	 * @name     listarEspecialidade
	 * 
	 * @method   GET
	 * @return   ResponseEntity<List<Object>>
	 * @produces application/JSON
	 * @info     Retornar uma lista de objetos cadastrados.
	*/
	public ResponseEntity<HttpResponse> listar(DocumentBase pDocumentBase) {
    	
    	try
		{
    		
    		List<?> listagem = service.listar(pDocumentBase);
    		
    		
    		return new ResponseEntity<HttpResponse>(new HttpResponse(listagem), HttpStatus.OK);
		}
    	catch(Exception e)
    	{
			LOGGER.error("[BaseRestController - listar]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	
	};
	
	/**
	 * @name     obter
	 * 
	 * @method   GET
	 * 
	 * @param    DocumentBase pDocumentBase
	 * 
	 * @return   ResponseEntity<HttpResponse>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Obter um objeto cadastrado no sistema
	*/
	public ResponseEntity<HttpResponse> obter(DocumentBase pDocumentBase) {
    	
		try
		{
			
	    	if(pDocumentBase == null || StringUtils.isEmpty(pDocumentBase.getId())){
	    		return new ResponseEntity<HttpResponse>(HttpStatus.PRECONDITION_FAILED);
	    	}

	    	DocumentBase documentBase = service.obter(pDocumentBase);
	    	
	    	return new ResponseEntity<HttpResponse>(new HttpResponse(documentBase) , HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[BaseRestController - obter]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	
	/**
	 * @name     inserir
	 * 
	 * @method   POST
	 * 
	 * @param    DocumentBase pDocumentBase
	 * @param    BindingResult bindingResult
	 * 
	 * @return   ResponseEntity<HttpResponse>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Inserir um novo dado no sistema.
	*/
	public ResponseEntity<HttpResponse> inserir(DocumentBase pDocumentBase, BindingResult bindingResult) {
    	
		try
		{
			
	        if (bindingResult.hasErrors() || StringUtils.isNotEmpty(pDocumentBase.getId())) {
	        	return new ResponseEntity<HttpResponse>(HttpStatus.PRECONDITION_FAILED);
	        }
	        
	        String id = service.inserir(pDocumentBase);
	       
	        DocumentBase documentBase = new DocumentBase();
	        documentBase.setId(id);

	    	return new ResponseEntity<HttpResponse>(new HttpResponse(documentBase), HttpStatus.OK);

		}
		catch(Exception e)
		{
			LOGGER.error("[BaseRestController - inserir]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	};
	
	/**
	 * @name     atualizar
	 * 
	 * @method   PUT
	 * 
	 * @param    DocumentBase pDocumentBase
	 * 
	 * @return   ResponseEntity<HttpResponse>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Atualizar um objeto j� existente no sistema
	*/
	public ResponseEntity<HttpResponse> atualizar(DocumentBase pDocumentBase, BindingResult bindingResult) {
    	
		try
		{
			
			if (bindingResult.hasErrors() || StringUtils.isEmpty(pDocumentBase.getId())) {
	        	return new ResponseEntity<HttpResponse>(HttpStatus.PRECONDITION_FAILED);
	        }
	        
			service.atualizar(pDocumentBase);
	    	
	    	return new ResponseEntity<HttpResponse>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[BaseRestController - atualizar]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	/**
	 * @name     excluir
	 * 
	 * @method   DELETE
	 * 
	 * @param    DocumentBase pDocumentBase
	 * 
	 * @return   ResponseEntity<HttpResponse>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Apagar um objeto j� existente no sistema
	*/
	public ResponseEntity<HttpResponse> excluir(DocumentBase pDocumentBase) {
    	
		try
		{
			
	    	if(pDocumentBase == null || StringUtils.isEmpty(pDocumentBase.getId())){
	    		return new ResponseEntity<HttpResponse>(HttpStatus.PRECONDITION_FAILED);
	    	}
	        
	        // Tornando o objeto INATIVO para a empresa.
	    	service.excluir(pDocumentBase);
	    	
	    	return new ResponseEntity<HttpResponse>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[BaseRestController - excluir]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	/**
	 * @name     inativar
	 * 
	 * @method   PUT
	 * 
	 * @param    DocumentBase pDocumentBase
	 * 
	 * @return   ResponseEntity<HttpResponse>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Inativar um objeto j� existente no sistema ( Atualizar status para INATIVO )
	*/
	public ResponseEntity<HttpResponse> inativar(DocumentBase pDocumentBase) {
    	
		try
		{
			
	    	if(pDocumentBase == null || StringUtils.isEmpty(pDocumentBase.getId())){
	    		return new ResponseEntity<HttpResponse>(HttpStatus.PRECONDITION_FAILED);
	    	}
	        
	        // Tornando o objeto INATIVO para a empresa.
	    	service.inativar(pDocumentBase);
	    	
	    	return new ResponseEntity<HttpResponse>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[BaseRestController - inativar]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	/**
	 * @name     ativar
	 * 
	 * @method   PUT
	 * 
	 * @param    DocumentBase pDocumentBase
	 * 
	 * @return   ResponseEntity<HttpResponse>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Ativar um objeto j� existente no sistema
	*/
	public ResponseEntity<HttpResponse> ativar(DocumentBase pDocumentBase) {
    	
		try
		{
			
	    	if(pDocumentBase == null || StringUtils.isEmpty(pDocumentBase.getId())){
	    		return new ResponseEntity<HttpResponse>(HttpStatus.PRECONDITION_FAILED);
	    	}
	        
	        // Tornando o especialidade INATIVO para a empresa.
	    	service.ativar(pDocumentBase);
	    	
	    	return new ResponseEntity<HttpResponse>(HttpStatus.OK);
	    
		}
		catch(Exception e)
		{
			LOGGER.error("[BaseRestController - ativar]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	};
	
	/**
	 * @name     listarInativo
	 * 
	 * @method   GET
	 * 
	 * @param    DocumentBase pDocumentBase
	 * 
	 * @return   ResponseEntity<List<Object>>
	 * 
	 * @produces application/JSON
	 * 
	 * @info     Retornar uma lista de objetos cadastrados e inativos.
	*/
	public ResponseEntity<HttpResponse> listarInativo(DocumentBase pDocumentBase) {
    	
    	try
		{
    		
    		List<?> listagem = service.listarInativo(pDocumentBase);
    		
    		
    		return new ResponseEntity<HttpResponse>(new HttpResponse(listagem), HttpStatus.OK);
		}
    	catch(Exception e)
    	{
			LOGGER.error("[BaseRestController - listarInativo]   MSG: " + e );
			return new ResponseEntity<HttpResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	
	};

};